public class TestStrings {
    public static void main(String[] args) {
        String example = "example.doc";
        if("example.doc".equals(example)){
            example = "example.bak";
        }
        System.out.println(example);

        // or use a StringBuilder
        StringBuilder example2 = new StringBuilder("example.doc");
        example2.replace(example2.length()-3, example2.length(), "bak");
        System.out.println("StringBuilder example = " + example2);

        //checking of 2 strings are equal
        String person1 = "Lola";
        String person2 = "Kason";
        if(person1.equals(person2)){
            System.out.println("Both people are the same");
        } else{
            //using ternary operation to see compare 2 names
            String furtherForward = (person1.compareTo(person2) > 0) ? person1 : person2;
            System.out.println(furtherForward + " is lexicolographically further forward in the dictionary");
        }

        //Find the number of time 'ow' occurs in String
        String sentence = "the quick brown fox swallowed down the lazy chicken";
        int counter = 0;
        for (int i = 0; i < sentence.length()-2; i++) {
            if(sentence.substring(i, i+2).equals("ow")){
            counter++;
            }
        }
        System.out.println("ow appears " + counter + " times");

        //Find if a string is a palindrome
        // quite interesting result, palidrome and reversePalidrome are still the same
        // value
        StringBuilder palidrome = new StringBuilder("livenotaweonevil");
        StringBuilder reversePalidrome = palidrome.reverse();
        if(palidrome.toString().equals(reversePalidrome.toString())){
            System.out.println("This is a palindrome." + reversePalidrome + '\n' + palidrome);
        }  else {
            System.out.println("This is not a palindrome");
        }
    }
}